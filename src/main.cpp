#include "CallbackAsyncSerial.h"
#include <stdio.h>
#include <iostream>
#include "ros/ros.h"
#include "std_msgs/Int32MultiArray.h"
#include "std_msgs/Float32MultiArray.h"
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"
#include "optoforce/Optoforce_4CH.h"
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

bool newData=false;
bool newPacket=false;
bool stopRequest=false;
int dataCount=0;
unsigned char DataBuffer[100];


typedef enum {
    Zero=0,
    Unzero=1,
    Update=5
}Zeroing;

typedef enum {
    Freq1000=0,
    Freq333=1,
    Freq100=2,
    Freq33=3
}Frequency;

typedef enum {
    BandOff=0,
    Band150=1,
    Band50=2,
    Band15=3
}Bandwidth;

typedef struct{
    float ForceX;
    float ForceY;
    float ForceZ;
} SensorReturn;

typedef struct{
    SensorReturn Force_FL;
    SensorReturn Force_FR;
    SensorReturn Force_BL;
    SensorReturn Force_BR;
} FullSensorReturn;


FullSensorReturn lastData;

void ProcessInput( const char *in, size_t size );
void SetZero(CallbackAsyncSerial *serial);
FullSensorReturn ParseData();
void SetConfig(CallbackAsyncSerial *serial,Zeroing ZeroConfig, Bandwidth BandConfig, Frequency FreqConfig);
void signal_callback_handler(int Value) ;

int main(int argc, char *argv[])
{
    ros::init(argc,argv, "dynarobinOptoforce");
    ros::NodeHandle n;

    //if (argc != 2) {
    //    std::cout<<"Please specify device name"<<std::endl;
    //    return 0;
    //}

    signal(SIGINT, signal_callback_handler);

    CallbackAsyncSerial *serial= new CallbackAsyncSerial();
    serial->setCallback( static_cast< const boost::function<void (const char*, unsigned int)> & > (boost::bind( &ProcessInput, _1, _2)) );
    serial->open(argv[1],115200);
    SetZero(serial);
    SetZero(serial);
    SetZero(serial);
    SetConfig(serial,Update, Band50, Freq1000);
    SetConfig(serial,Update, Band50, Freq1000);
    SetZero(serial);
    std::cout<<"Trying to open port!"<<std::endl;
    if(serial->isOpen()){
        std::cout<<"Device opened"<<std::endl;
        ros::Publisher optoforce_pub = n.advertise<std_msgs::Float32MultiArray>("Dynarobin_Optoforce", 10000);
        ros::Publisher optoforce_pub_4CH = n.advertise<optoforce::Optoforce_4CH>("Dynarobin_Optoforce_4CH", 10000);
        ros::Rate loop_rate(1);

        int count = 0;

        while (ros::ok())
        {
            if (newData){
                std_msgs::Float32MultiArray msg;
                optoforce::Optoforce_4CH msg_4CH;

                msg.data.push_back(lastData.Force_FL.ForceX);
                msg.data.push_back(lastData.Force_FL.ForceY);
                msg.data.push_back(lastData.Force_FL.ForceZ);

                msg.data.push_back(lastData.Force_FR.ForceX);
                msg.data.push_back(lastData.Force_FR.ForceY);
                msg.data.push_back(lastData.Force_FR.ForceZ);

                msg.data.push_back(lastData.Force_BL.ForceX);
                msg.data.push_back(lastData.Force_BL.ForceY);
                msg.data.push_back(lastData.Force_BL.ForceZ);

                msg.data.push_back(lastData.Force_BR.ForceX);
                msg.data.push_back(lastData.Force_BR.ForceY);
                msg.data.push_back(lastData.Force_BR.ForceZ);

		msg_4CH.FL_X=lastData.Force_FL.ForceX;
		msg_4CH.FL_Y=lastData.Force_FL.ForceY;
		msg_4CH.FL_Z=lastData.Force_FL.ForceZ;

		msg_4CH.FR_X=lastData.Force_FR.ForceX;
		msg_4CH.FR_Y=lastData.Force_FR.ForceY;
		msg_4CH.FR_Z=lastData.Force_FR.ForceZ;

		msg_4CH.BL_X=lastData.Force_BL.ForceX;
		msg_4CH.BL_Y=lastData.Force_BL.ForceY;
		msg_4CH.BL_Z=lastData.Force_BL.ForceZ;

		msg_4CH.BR_X=lastData.Force_BR.ForceX;
		msg_4CH.BR_Y=lastData.Force_BR.ForceY;
		msg_4CH.BR_Z=lastData.Force_BR.ForceZ;

                optoforce_pub.publish(msg);
                optoforce_pub_4CH.publish(msg_4CH);

               newData=false;
            }
            ++count;
            if (stopRequest) {
                std::cout<<"Closing"<<std::endl;
                serial->close();
                return 0;
            }
        }
    }

    serial->close();
    return 0;
}

void signal_callback_handler(int Value) {
    stopRequest=true;
}

void SetConfig(CallbackAsyncSerial *serial,Zeroing ZeroConfig, Bandwidth BandConfig, Frequency FreqConfig){
    if (!serial->isOpen()) return;
    char Data=0;
    Data=((ZeroConfig)<<5)|((BandConfig)<<4)|((FreqConfig)<<1)|0x01;
    Data=0b10100110;
    serial->write(&Data,1);
    std::cout<<(int)Data<<std::endl;
}
void SetZero(CallbackAsyncSerial *serial){
    if (!serial->isOpen()) return;
    char Data=0;
    serial->write(&Data,1);
}

void ProcessInput( const char *in, size_t size )
{
    static char lastChar;
            std::cout<<"New Char"<<std::endl;
    for (int i=0;i<size;i++){
        char newChar=in[i];
        if(lastChar==55 && newChar==94){
            std::cout<<"Data count:"<<dataCount<<std::endl;
            if(dataCount>44) lastData = ParseData();
            // Ovdje parsaj podatak
            dataCount=0;
        }
        if (dataCount>95){
            dataCount=0;
        }
        DataBuffer[dataCount]=newChar;
        dataCount++;
        lastChar=newChar;
    }



}
FullSensorReturn ParseData(){
    FullSensorReturn out;
    SensorReturn FL,FR,BL,BR;
    static SensorReturn FL_initial, FR_initial, BL_initial, BR_initial;
    static bool Initialized=false;
    uint16_t S1,S2,S3,S4;

    newData=true;
    S1=(((unsigned int)DataBuffer[2])<<8) | ((unsigned int)DataBuffer[3]);
    S2=(((unsigned int)DataBuffer[4])<<8) | ((unsigned int)DataBuffer[5]);
    S2=(((unsigned int)DataBuffer[6])<<8) | ((unsigned int)DataBuffer[7]);
    S3=(((unsigned int)DataBuffer[8])<<8) | ((unsigned int)DataBuffer[9]);

    FL.ForceX=(S1-S3)*1.0-FL_initial.ForceX;
    FL.ForceY=(S4-S2)*1.0-FL_initial.ForceY;
    FL.ForceZ=(S1+S2+S3+S4)/4.0-FL_initial.ForceZ;

    S1=(((unsigned int)DataBuffer[12])<<8) | ((unsigned int)DataBuffer[13]);
    S2=(((unsigned int)DataBuffer[14])<<8) | ((unsigned int)DataBuffer[15]);
    S2=(((unsigned int)DataBuffer[16])<<8) | ((unsigned int)DataBuffer[17]);
    S3=(((unsigned int)DataBuffer[18])<<8) | ((unsigned int)DataBuffer[19]);

    FR.ForceX=(S1-S3)*1.0-FR_initial.ForceX;
    FR.ForceY=(S4-S2)*1.0-FR_initial.ForceY;
    FR.ForceZ=(S1+S2+S3+S4)/4.0-FR_initial.ForceZ;

    S1=(((unsigned int)DataBuffer[22])<<8) | ((unsigned int)DataBuffer[23]);
    S2=(((unsigned int)DataBuffer[24])<<8) | ((unsigned int)DataBuffer[25]);
    S2=(((unsigned int)DataBuffer[26])<<8) | ((unsigned int)DataBuffer[27]);
    S3=(((unsigned int)DataBuffer[28])<<8) | ((unsigned int)DataBuffer[29]);

    BL.ForceX=(S1-S3)*1.0-BL_initial.ForceX;
    BL.ForceY=(S4-S2)*1.0-BL_initial.ForceY;
    BL.ForceZ=(S1+S2+S3+S4)/4.0-BL_initial.ForceZ;

    S1=(((unsigned int)DataBuffer[32])<<8) | ((unsigned int)DataBuffer[33]);
    S2=(((unsigned int)DataBuffer[34])<<8) | ((unsigned int)DataBuffer[35]);
    S2=(((unsigned int)DataBuffer[36])<<8) | ((unsigned int)DataBuffer[37]);
    S3=(((unsigned int)DataBuffer[38])<<8) | ((unsigned int)DataBuffer[39]);

    BR.ForceX=(S1-S3)*1.0-BR_initial.ForceX;
    BR.ForceY=(S4-S2)*1.0-BR_initial.ForceY;
    BR.ForceZ=(S1+S2+S3+S4)/4.0-BR_initial.ForceZ;

    // Pretvorba u prave lokacije
    out.Force_FL=FR;
    out.Force_FR=BL;
    out.Force_BL=FL;
    out.Force_BR=BR;
    std::cout<<"Front left -> x: "<<out.Force_FL.ForceX<<" y: "<<out.Force_FL.ForceY<<" z: "<<out.Force_FL.ForceZ<<std::endl;
    std::cout<<"Front right -> x: "<<out.Force_FR.ForceX<<" y: "<<out.Force_FR.ForceY<<" z: "<<out.Force_FR.ForceZ<<std::endl;
    std::cout<<"Back left -> x: "<<out.Force_BL.ForceX<<" y: "<<out.Force_BL.ForceY<<" z: "<<out.Force_BL.ForceZ<<std::endl;
    std::cout<<"Back right -> x: "<<out.Force_BR.ForceX<<" y: "<<out.Force_BR.ForceY<<" z: "<<out.Force_BR.ForceZ<<std::endl;

    if (!Initialized){
	FL_initial.ForceX=FL.ForceX;
    	FL_initial.ForceY=FL.ForceY;
    	FL_initial.ForceZ=FL.ForceZ;
	FR_initial.ForceX=FR.ForceX;
    	FR_initial.ForceY=FR.ForceY;
    	FR_initial.ForceZ=FR.ForceZ;
	BL_initial.ForceX=BL.ForceX;
    	BL_initial.ForceY=BL.ForceY;
    	BL_initial.ForceZ=BL.ForceZ;
	BR_initial.ForceX=BR.ForceX;
    	BR_initial.ForceY=BR.ForceY;
    	BR_initial.ForceZ=BR.ForceZ;
	Initialized=true;
    }
    


    return out;
}
