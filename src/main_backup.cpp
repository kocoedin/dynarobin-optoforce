#include <omd/opto.h>
#include <omd/optoports.h>
#include <stdio.h>
#include <iostream>
#include <QTime>
#include <QtCore/QCoreApplication>
#include <QDebug>
#include <QThread>
#include "ros/ros.h"
#include "std_msgs/Int32MultiArray.h"
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"


class Sleeper : public QThread
{
public:
    static void usleep(unsigned long usecs){QThread::usleep(usecs);}
    static void msleep(unsigned long msecs){QThread::msleep(msecs);}
    static void sleep(unsigned long secs){QThread::sleep(secs);}
};


int main(int argc, char *argv[])
{
    ros::init(argc,argv, "dynarobinOptoforce");
    ros::NodeHandle n;

    QCoreApplication a(argc,argv);


    ros::Publisher optoforce_pub = n.advertise<std_msgs::Int32MultiArray>("Dynarobin_Optoforce", 1000);


    OptoDAQ daq;
    OptoPorts ports;

    OPort* portlist=ports.listPorts(true);

    ros::Rate loop_rate(1000);

    int count = 0;

    if (ports.getLastSize()>0)
    {
        for (int i=0;i<ports.getLastSize();i++){
           std::cout<<portlist[i].name<<std::endl;
        }
        daq.open(portlist[0]);
        daq.zeroAll();


        if (daq.getVersion()!=_95) // It is a 3D sensor
        {
            while (ros::ok())
            {
                OptoPackage pack3D;
                int size=daq.read(pack3D,0);	// Reading Sensor #0 (up to 16 Sensors)

                if (size!=0){
                    std_msgs::Int32MultiArray msg;
                    msg.data.push_back(pack3D.x);
                    msg.data.push_back(pack3D.y);
                    msg.data.push_back(pack3D.z);
                    optoforce_pub.publish(msg);

                    std::cout<<"x: "<<pack3D.x<<" y: "<<pack3D.y<<" z: "<<pack3D.z<<" size: "<<size<<std::endl;
                }
                //Sleeper::msleep(10);
                //ros::spinOnce();
                loop_rate.sleep();
                ++count;

            }
        }
        else					  // It is a 6D sensor
        {
            OptoPackage6D pack6D;
            int size=daq.read6D(pack6D,false);
            std::cout<<"Fx: "<<pack6D.Fx<<" Fy: "<<pack6D.Fy<<" Fz: "<<pack6D.Fz<<" ";
            std::cout<<"Tx: "<<pack6D.Tx<<" Ty: "<<pack6D.Ty<<" Tz: "<<pack6D.Tz<<std::endl;
        }
        daq.close();
    }
    else
    {
        std::cout<<"No sensor available"<<std::endl;
    }
    char key;
    std::cin>>key;


    return a.exec();
}

